# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  subject { FactoryBot.build(:user) }

  it { is_expected.to respond_to(:name) }
  it { is_expected.to respond_to(:password_hash) }

  describe 'when name is not present' do
    before { subject.name = ' ' }

    it { expect(subject.errors[:name]).to be_truthy }
  end

  describe 'when password_hash is not present' do
    before { subject.password_hash = ' ' }

    it { expect(subject.errors[:password_hash]).to be_truthy }
  end

  describe 'when password is not present' do
    before { subject.password = ' ' }

    it { expect(subject.errors[:password_hash]).to be_truthy }
  end

  describe 'when password is not present' do
    before { subject.password = 'somepass' }

    it { expect(subject.errors[:password_hash]).to eq([]) }
    it { expect(subject.password).to be_truthy }
  end
end
