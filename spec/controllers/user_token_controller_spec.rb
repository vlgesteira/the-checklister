# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserTokenController, type: :controller do
  describe 'POST #create' do
    let(:name) { 'anyname' }
    let(:correct_password) { 'somepass' }

    before { FactoryBot.create(:user, name: name, password: correct_password) }

    context 'with incorrect params' do
      let(:incorrect_password) { 'nonepass' }
      let(:user) { { 'user' => { name: name, password: incorrect_password } } }
      before do
        post :create, params: user
      end

      it 'does not return a token' do
        expect(response.body).to be_empty
      end
    end

    context 'with correct params' do
      let(:user) { { 'user' => { name: name, password: correct_password } } }
      before do
        post :create, params: user
      end

      it 'does not return a token' do
        expect(response.headers.as_json.keys).to include('access_token')
      end
    end
  end
end
