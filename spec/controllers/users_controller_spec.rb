# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe '#send(:create_secure_user, user)' do
    let(:user) { FactoryBot.build(:user) }

    it 'does not create a secure user with invalid params' do
      expect(subject.send(:create_secure_user, name: 'none', password: nil)).to be_falsey
    end

    it 'creates a secure user' do
      expect(subject.send(:create_secure_user, user.as_json)).to be_truthy
    end
  end
end
