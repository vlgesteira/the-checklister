# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    name { 'someone' }
    password_hash { 'some_hash' }
  end
end
