# frozen_string_literal: true

Rails.application.routes.draw do
  post 'auth' => 'user_token#create'
  resources :users
  root to: 'pages#root'

  resources :users do
    collection do
      post :auth
    end
  end
end
