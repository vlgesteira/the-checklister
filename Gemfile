# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

gem 'mysql2', '>= 0.4.4', '< 0.6.0'
gem 'puma', '3.11'
gem 'rails', '~> 5.2.2'
gem 'uglifier', '>= 1.3.0'
gem 'webpacker'

gem 'bcrypt', '~> 3.1'
gem 'jbuilder', '~> 2.5'
gem 'jwt'
gem 'knock'
gem 'mini_magick', '~> 4.8'
gem 'redis', '~> 4.0'

group :development, :test do
  gem 'bootsnap', '>= 1.1.0', require: false
  gem 'byebug'
  gem 'rubocop', require: false
end

group :development do
  gem 'factory_bot_rails'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara'
  gem 'chromedriver-helper'
  gem 'database_cleaner'
  gem 'faker'
  gem 'rspec-rails'
  gem 'selenium-webdriver'
  gem 'simplecov', require: false
end
