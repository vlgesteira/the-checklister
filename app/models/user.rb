# frozen_string_literal: true

require 'bcrypt'

class User < ApplicationRecord
  include BCrypt

  validates :name, :password_hash, presence: true
  validates :name, uniqueness: true

  def password
    @password ||= Password.new(password_hash)
  end

  def password=(new_password)
    return if new_password.blank?

    @password = Password.create(new_password)
    self.password_hash = @password
  end
end
