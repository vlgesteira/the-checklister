# frozen_string_literal: true

class UserTokenController < Knock::AuthTokenController
  # '/auth'
  def create
    raise Knock.not_found_exception_class unless entity.present?

    headers['access_token'] = Knock::AuthToken.new(payload: { sub: @entity.id, expires_at: Time.zone.now }).token
    render json: entity.as_json
  end

  def authenticate
    # raise Knock.not_found_exception_class unless entity.present?
  end

  private

  def find_user
    user = User.find_by_name(params[:user][:name])
    password = params[:user][:password]
    return if user.nil? || user.password != password

    user
  end

  def entity
    @entity ||= find_user
  end

  def auth_params
    params.require(:user).permit(:name, :password)
  end
end
