# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Knock::Authenticable
  before_action :authenticate_and_set_token

  protected

  def authenticate_and_set_token
    authenticate_user
    if current_user.present?
      headers['access_token'] = Knock::AuthToken.new(payload: { sub: current_user.id }).token
    end
  end
end
